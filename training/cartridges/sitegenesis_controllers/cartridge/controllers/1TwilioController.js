var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Resource = require('dw/web/Resource');

function addSubscriber() {
    const twilioPhoneNumber = CustomObjectMgr.getCustomObject("twilio-sms-phone", 'account').custom.phone;
    let form = app.getForm('subscribe');
    let phone = form.object.phone.value;
    let name = form.object.name.value;
    let targetProduct = form.object.productId.value;
    let UrlObject = {
        From: twilioPhoneNumber,
        To: phone,
        Body:'Hi, ' + name + ', When ' + targetProduct + ' is available, You will receive an SMS'
    }
  
    form.handleAction({
        subscribe: function () {
            let subscribedProduct = CustomObjectMgr.getCustomObject('exercise2-second', targetProduct)
            let smsService = require('int_twilio/cartridge/scripts/twilio');

            //If the product is already in DB
            if (subscribedProduct) {
                let subscribedUser = JSON.parse(CustomObjectMgr.getCustomObject('exercise2-second', targetProduct).custom.subscriber);
                //Chek the user`s phone is subscribed to the product
                if (subscribedUser[phone]) {
                     errorAddNewSubscriber('You are already subscribed!');
                } else {
                    // if the phone is not in DB, send sms
                    let smsApiUrlparams = smsService.createUrlRequest(UrlObject);
                    let smsResponse = smsService.sendSms(smsApiUrlparams);
                    if (smsResponse.success) {
                        addNewSubscriber(subscribedUser);
                    } else {
                        errorAddNewSubscriber('The Phone is not valid!');
                    }
                }
            } else {
                //If the product is not in DB
                let smsApiUrlparams = smsService.createUrlRequest(UrlObject);
                let smsResponse = smsService.sendSms(smsApiUrlparams);
                
                if (smsResponse.success) {
                    addNewSubscriber(null, phone, name, targetProduct);
                } else {
                    errorAddNewSubscriber('The Phone is not valid!');
                }
            }
        },
        error: function () {
            response.redirect(URLUtils.https('Product', 'pid', 'test-product2-2'));
        }
    })
}

function addNewSubscriber(subscriberObject, phone, name, targetProduct) {
    let newSubscriber = {};

    if (subscriberObject != null) {
        newSubscriber = subriber;
        newSubscriber[phone] = {
            name: name
        };
    } else {
        newSubscriber[phone] = {
            name: name
        };
    }
    Transaction.wrap(function () {
        CustomObjectMgr.createCustomObject('exercise2-second', targetProduct).custom.subscriber = JSON.stringify(newSubscriber);
    })
    app.getView().render('product/productsubscribesuccess');
}

function errorAddNewSubscriber(errorMessage) {
    app.getView({
        errMsg: errorMessage
    }).render('product/productsubscribeerror');
}

exports.Start = guard.ensure(['post'], addSubscriber);
