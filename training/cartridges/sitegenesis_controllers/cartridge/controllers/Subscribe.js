var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var CustomObjectMgr = require('dw/object/CustomObjectMgr')
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

function start() {
    var subscribeForm = session.forms.subscribe;
    app.getForm('subscribe').handleAction({
        subscribe: function () {
            Transaction.wrap(function () {
                let CO = CustomObjectMgr.getCustomObject('exercise2', session.forms.subscribe.phone.value);
                session.forms.subscribe.copyTo(CO);
                });

                let json = JSON.stringify({success:true});
                response.writer.print(json);
                return;
        },
        cancel:  function() {
            response.redirect(URLUtils.https('NewsletterList'));
             return;
        }
    })
    
}


exports.Start = guard.ensure(['post'], start);

