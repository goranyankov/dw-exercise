module.exports.init = function () {
	if ($('.address_add').length > 0) {
		addAddress();
	} else if ($('.address_all').length > 0) {
		allAddress();
	}

	function allAddress() {
		let searchType = $('#type');
		let fields = $('#fields');
		//sort by CO attribut
		fields.on('change', function () {
			sort();
		})
		//sort by CO asc or desc
		searchType.on('change', function () {
			sort();
		})

		//add change event to all inputs
		//but I can do it for type inpyt[type=checkbox]
		$(document).on('change', 'input[type="checkbox"]', function (e) {
			let box = $(e.target);
			if (box.prop('checked') == true) {
				hideShowFun(box, 'hide');
			} else {
				hideShowFun(box, 'show');
			}
		});

		//sort function
		function sort() {
			let attr = fields.val();
			let typeOfSort = searchType.val();

			//ajax dosn`t send data
			//Addres-Sort use query
			let req = {
				method: "GET",
				url: Urls.addAddressSort + `?name=${attr}&sort=${typeOfSort}`,
				//Addres-Sort return new tabel
				success: (data) => {
					let table = $('table');
					let newTable = $(data)[2]
					let div = table.parent();
					div.empty();
					div.append(newTable);
				}
			}

			$.ajax(req);
		}

		//if the checkbox is clicked the function hide the row of the tabl
		//if the checkbox isn`t clicked the function show tthe row of the tabl
		function hideShowFun(jElement, action) {
			let parent = jElement.parent().parent();
			let children = parent.children();
			children.each(function (i) {
				if (i != 0) {
					if (action === 'hide') {
						$(this).hide();
					} else {
						$(this).show();
					}
				}
			})
		}
	}

	function addAddress() {
		//add eventL. to country
		let country = $('#dwfrm_address_country')

		country.change(function (e) {
			changeState(e);
		})

		let email = $('#dwfrm_address_email');
		email.focusout(function (e) {
			isEmailExist(e);
		})

		function creatElement(jQueryElement, string) {
			removeSpan(jQueryElement)
			jQueryElement.addClass('input-text required error');
			let errSpan = $(`<span id="dwfrm_address_email-error" class="error">${string}</span>`)
			let inputParent = jQueryElement.parent().append(errSpan);
			return 1;
		}

		// //remove err span bottom the input
		function removeSpan(element) {
			if (element.next().is('span')) {
				element.next('span').remove();
			}
		}

		// if Country != US, input Stite.hide()
		function changeState(e) {
			let country = $(e.target).find(":selected").text();
			let stateParent = $('#dwfrm_address_state').parent().parent();
			let isTrue = (country !== 'United States');
			if (country !== 'United States') {
				stateParent.hide();
			} else {
				stateParent.show();
			}

		}


		// //Check email 
		function isEmailExist() {
			//Create Object from ajax
			//if Address-email return json with obj. success=true, email already exist and form dosn`t send
			//user must add dif. email
			let req = {
				method: "GET",
				url: Urls.addAddressEmail + `?email=${email.val()}`,
				success: (data) => {
					if (data.success == false) {
						creatElement(email, 'already exists');
						//$('.button').prop( "disabled", true );
					} else {
						removeSpan(email)
					}
				}
			}

			$.ajax(req);
		}
	}
}