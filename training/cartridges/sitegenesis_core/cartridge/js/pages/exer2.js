'use strict';
module.exports.init = function () {
   $('#submitbutton').click(function(e) {
        e.preventDefault();
        let name = $('#dwfrm_subscribe_name').val();
        let phone = $('#dwfrm_subscribe_phone').val();
        let product = $('input[name=product]').val();
        if(inputIsValid(name, 'string') &&  inputIsValid(phone, 'int') && $('#dwfrm_subscribe').valid()) {
            let url = window.location.pathname;
            let str = url.split('/').pop();
            let index = str.indexOf('.html');
            let news =  str.substring(0,index);
            let req = {
                method: "POST",
                url: Urls.addPreorderSubscribe,
                data: $('form').serialize()  + '&' + 'dwfrm_subscribe_subscribe' + '=subscribe',
                success: (data) => {
                    let divPreorder = $('.preoreder-form');
                    let subscribeSuccess = $(data)[2];
                    divPreorder.empty().append(subscribeSuccess);
                   
                }
            }
         $.ajax(req);
        } 
        
   })

   function inputIsValid(input, type) {
       if(input === '') {
           return false;
       } else if (type === 'string' && (/[0-9!?]+/g).test(input)) {
            return false
       } else if (type === 'int' && (/[a-zA-z]+/g).test(input)) {
           return false
       }
       return true
   }

   let p = $('.payment-method-options form-indent');
   console.log(p);

}