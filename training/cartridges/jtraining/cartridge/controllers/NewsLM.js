'use strict';

var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');
var URLUtils = require('dw/web/URLUtils');
var form = require('dw/web/FormGroup');
var Transaction = require('dw/system/Transaction');

function start() {
    var addressForm = app.getForm('profile').clear();

    app.getView({
        Action: 'add',
        ContinueURL: URLUtils.https('NewsLM-HandleForm')
    }).render('account/addressbook/addressdetails');
}

function handleForm() {
    var Address;
    var success;
    var message;
    
    Address = app.getModel('Address');
    var addressForm = app.getForm('profile')
    //First get the form with the name newsletter
    //Second handle differenet Action
    addressForm.handleAction({
        create: function () {
            if (!session.forms.profile.address.valid ||
                !Address.create(session.forms.profile.address)) {
                response.redirect(URLUtils.https('Address-Add'));
                success = false;
            }
            success = true;
        },
        edit: function () {
            if (!session.forms.profile.address.valid) {
                success = false;
                message = 'Form is invalid';
            }
            try {
                Address.update(request.httpParameterMap.addressid.value, session.forms.profile.address);
                success = true;
            } catch (e) {
                success = false;
                message = e.message;
            }
        }

    });
}

/** Shows the template page. */
exports.Start = guard.ensure(['get'], start);
exports.HandleForm = guard.ensure(['post'], handleForm);;