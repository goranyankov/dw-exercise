var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
const basketMgr = require('dw/order/BasketMgr'); 

let start = function () {
    var basket = basketMgr.getCurrentBasket();
   
    if(basket.productQuantityTotal !== null || basket.productQuantityTotal !== 0) {
        render(basket);
    } else {
        renderError()
    }

}

let deleteBasketOne = function () {
    var basket = basketMgr.getCurrentBasket();
    basketMgr.deleteBasket(basket);

        renderError()
    
}

let render = function (myProduct) {
    app.getView({
        myBasket: myProduct,
    }).render('showBasket');
}


let renderError = function (myPid) {
    response.getWriter().println('Cart is empty');
}


let debug = function(object) {
    response.setContentType('application/json');  
     let json = JSON.stringify(object);   
     response.writer.print(json);  
}
exports.Start = guard.ensure(['get'], start);
exports.Remove = guard.ensure([], deleteBasketOne);