var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');

var preferencesForm = session.forms.preferences;
//start function 
let start = function () {
    preferencesForm.clearFormElement();
    //Global customer return the current profile
    let profile = customer.profile
    //Map form with the profile data
    preferencesForm.copyFrom(profile); 
    render('EditProfile-End');
}

//On submit 
let handleForm = function () {
    //Get the Form and handle information
    app.getForm('preferences').handleAction({
        //submit buton apply
        apply: function() {  
            //Start transaction
            Transaction.wrap(function () {
                //coppy information from the form to the Profile System Object
                preferencesForm.copyTo(customer.profile);
            })

            //redirect to the startpoint
            response.redirect(URLUtils.https('EditProfile'));
        }
    })
}

// render success function
let render = function (endpoint) {
    app.getView({
        ContinueURL: URLUtils.https(endpoint)
    }).render('editpreferences');
}

// render error function
let renderError = function (myPid) {
    response.getWriter().println('Error product with Id = ' + myPid + ' dosn`t not existed');
}


exports.Start = guard.ensure(['get', 'loggedIn'], start);
exports.End = guard.ensure(['post', 'loggedIn'], handleForm);