var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard'); 
var URLUtils = require('dw/web/URLUtils')

let start = function () {
    session.forms.newsletter.clearFormElement();

    app.getView({
        ContinueURL:  URLUtils.https('newsletter2') 
    }).render('newsletter/newslettersignup');
}

exports.Start = guard.ensure(['get'], start);
