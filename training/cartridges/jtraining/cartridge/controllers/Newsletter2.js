'use strict';

var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');
var URLUtils = require('dw/web/URLUtils');
var form = require('dw/web/FormGroup');
var Transaction = require('dw/system/Transaction');

function start() {
    session.forms.newsletter.clearFormElement();
    app.getView({
        ContinueURL: URLUtils.https('Newsletter2-HandleForm')
    }).render('newsletter/newslettersignup');
}

function handleForm() {

    var newsletterForm = session.forms.newsletter;

    //First get the form with the name newsletter
    //Second handle differenet Action
    app.getForm('newsletter').handleAction({
        //reset the form
        cancel: function () {
            app.getForm('newsletter').clear();
            //redirect to the form
            response.redirect(URLUtils.https('Newsletter2'));
        },
        //submit form
        subscribe: function () {
            //require CustomObjectMgr
            var CustomObjectMgr = require('dw/object/CustomObjectMgr');
            //Start transaction    
            Transaction.wrap(function () {
                //create Custom Object createCustomObject method has to params Custom OBJ and key value.
                var CO = CustomObjectMgr.createCustomObject('NewsletterSubscription', newsletterForm.email.value);
                //save the result form bind to the custom Object
                newsletterForm.copyTo(CO);
            });

            //render view
            app.getView().render('newsletter/newslettersuccess');
        }
    });
}

/** Shows the template page. */
exports.Start = guard.ensure(['get'], start);
exports.HandleForm = guard.ensure(['post'], handleForm);;