var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var CustomObjectMgr = require('dw/object/CustomObjectMgr')
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

function start() {
    let allSubscription = CustomObjectMgr.getAllCustomObjects('NewsletterSubscription');
    render(allSubscription)
}

function removeSubscriber() {
    let email = request.httpParameterMap.email;

    Transaction.wrap(function () {
        let CO = CustomObjectMgr.getCustomObject('NewsletterSubscription', email)
        CustomObjectMgr.remove(CO);
    })


    let allSubscription = CustomObjectMgr.getAllCustomObjects('NewsletterSubscription');
    render(allSubscription)
}

function editSingleSubscriber() {
    let email = request.httpParameterMap.email;
    let newsLetterForm = app.getForm('newsletter');
    let CO = CustomObjectMgr.getCustomObject('NewsletterSubscription', email)
    newsLetterForm.clear()
    newsLetterForm.copyFrom(CO);
    app.getView({
        ContinueURL: URLUtils.https('NewsletterList-PostEdit')
    }).render('newsletter/newslettersignup');
}

function postEdit() {
    var newsLetterForm = session.forms.newsletter;
    app.getForm('newsletter').handleAction({
        subscribe: function () {
            Transaction.wrap(function () {
                let CO = CustomObjectMgr.getCustomObject('NewsletterSubscription', session.forms.newsletter.email.value);
                session.forms.newsletter.copyTo(CO);
                });
                response.redirect(URLUtils.https('NewsletterList'));
                return;
        },
        cancel:  function() {
            response.redirect(URLUtils.https('NewsletterList'));
             return;
        }
    })
    
}



function render(allSubscription) {
    app.getView({
        'test': '1222',
        allSubscription: allSubscription
    }).render('allSubcription');
}


exports.Start = guard.ensure(['get'], start);
exports.Remove = guard.ensure(['get'], removeSubscriber);
exports.Edit = guard.ensure(['get'], editSingleSubscriber);
exports.PostEdit = guard.ensure(['post'], postEdit);