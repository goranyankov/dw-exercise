var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var URLUtils = require('dw/web/URLUtils')
require('dw/system'); 
require('dw/object'); 
require('dw/web'); 

let start = function () {
    session.forms.newsletter.clearFormElement();

    app.getView({
        ContinueURL:  URLUtils.https('Newsletter-HandleForm') 
    }).render('newsletter/newslettersignup');
}


let handleForm = function() {
    var TriggeredAction = request.triggeredFormAction;
    if (TriggeredAction != null) {
        if (TriggeredAction.formId == 'subscribe') {
            var newsletterForm = session.forms.newsletter;
           //success triggered

           //debug form
          // response.getWriter().println('Hello World from pipeline             controllers!'+newsletterForm); 
            app.getView({
                "email": newsletterForm.email.value,
                "ivan": "ivan"
            }).render('newsletter/newslettersuccess');
            return true;
        } else {
            //if formId dosn`t match
            errorHandleForm();
        }
     }
     else {
         //if Triggered === null
        errorHandleForm();
    }   
}

let errorHandleForm = function() {
   // let obj = require('./ErrorPage');
    return app.getView({
        "email": 'Sorry Dudeeee'
    }).render('newsletter/newslettererror');
}
exports.Start = guard.ensure(['get'], start);
exports.HandleForm = guard.ensure(['post'], handleForm);