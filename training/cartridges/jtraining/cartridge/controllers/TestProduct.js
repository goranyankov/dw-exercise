var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');

//start function 
let start = function () {
    //get pid query
    var CurrentHttpParameterMap = request.httpParameterMap;
    var myPid=CurrentHttpParameterMap.pid.stringValue;

    //get product by pid query string
    let myProduct = dw.catalog.ProductMgr.getProduct(myPid)

    //check if the product exists
    if(myProduct) { 
        //call render success page
        render(myProduct)
        return true;
    } else {
        // call render error page
        renderError(myPid);
    }
}

// render success function
let render = function (myProduct) {
    app.getView({
        myProduct: myProduct,
    }).render('productfound');
}

// render error function
let renderError = function (myPid) {
    response.getWriter().println('Error product with Id = ' + myPid + ' dosn`t not existed');
}

let debug = function(object) {

      response.setContentType('application/json');  
       let json = JSON.stringify(object);   
       response.writer.print(json);  
}

exports.Start = guard.ensure(['get'], start);