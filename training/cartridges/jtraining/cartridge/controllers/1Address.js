var app = require('sitegenesis_controllers/cartridge/scripts/app');
var guard = require('sitegenesis_controllers/cartridge/scripts/guard');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var params = request.httpParameterMap;
var Countries = require('sitegenesis_core/cartridge/scripts/util/Countries');


//index (Start) router Address 
let start = function () {
    app.getView({ 
        ContinueURL: URLUtils.https('Address-Add')
    }).render('address/address');
}

//router Address-All get all custom object of type BookAdres (with one s ':)' )
let allCustomObject = function () {
    let allAddress = functionSort();
    let CustomOb = CustomObjectMgr.getCustomObject("AddresBook", 'goran@abv.bg').describe().getAttributeDefinitions();
    app.getView({
        CustomOb: CustomOb,
        allAddress: allAddress,
    }).render('address/all-address');
}

//router Address-Add handel form
function handleForm() {
    app.getForm('address').handleAction({
        subscribe: function () {
            Transaction.wrap(function () {
                var CO = CustomObjectMgr.createCustomObject('AddresBook', session.forms.address.email.value);
                session.forms.address.copyTo(CO);
            })
            response.redirect(URLUtils.https('Address-All'));
        },
        error: function() {
            app.getView({
                ContinueURL: URLUtils.https('Address-Add')
            }).render('address/address');
        }
    })
}


//router Address-Email handel form
let checkEmail = function () {
    let email = request.httpParameterMap.email;
    //Checks whether there is an object with the same email (CO or null)
    let CustomObjectField = CustomObjectMgr.getCustomObject('NewsletterSubscription', email)
 
    // Return JSON Object success:
    // true, if the object dosn`t exist and form can be send
    // fasle, if the object exist and form can`t be send
    if (CustomObjectField) {
        response.setContentType('application/json');
        let json = JSON.stringify({
            success: false
        });
        response.writer.print(json);
    } else {
        response.setContentType('application/json');
        let json = JSON.stringify({
            success: true
        });
        response.writer.print(json);
    }
}

//router Address-Sort (GET) 
let sort = function () {
    let allAddress = functionSort();
    let CustomOb = CustomObjectMgr.getCustomObject("AddresBook", 'goran@abv.bg').describe().getAttributeDefinitions();
    app.getView({
        allAddress: allAddress,
    }).render('address/tabel');
}


//return sort object
let functionSort = function () {
    let customObjectAtr = params.name.stringValue;
    let customObjectName = 'custom.' + params.name.stringValue;
    let sortType = params.sort.stringValue;
    let allAddress;
    if (!customObjectAtr) {
        return allAddress = CustomObjectMgr.queryCustomObjects('AddresBook', '', 'creationDate asc', null);
    }
    return allAddress = CustomObjectMgr.queryCustomObjects('AddresBook', '', customObjectName + ' ' + sortType, null);
}
exports.Start = guard.ensure(['get'], start);
exports.All = guard.ensure(['get'], allCustomObject);
exports.Add = guard.ensure(['post'], handleForm);
exports.Email = guard.ensure(['get'], checkEmail);
exports.Sort = guard.ensure(['get'], sort);