'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat-util');

var pkg = require('./package.json');
var paths = pkg.paths;

var merge = require('merge-stream');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');

gulp.task('css', function() {
    var streams = merge();
    paths.css.forEach(function(path) {
        streams.add(gulp.src(path.src + '*.scss')
            .pipe(sourcemaps.init())
            .pipe(gulpif(argv.uglify, sass({
                outputStyle: 'compressed'
            }), sass()).on('error', sass.logError))
            .pipe(prefix({
                cascade: true
            }))
            .pipe(gulpif(argv.sourcemaps, sourcemaps.write('./')))
            .pipe(gulp.dest(path.dest)));
    });
    return streams;
});

var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var uglify = require('gulp-uglify-es').default;
var xtend = require('xtend');

var watching = false;
gulp.task('enable-watch-mode', function() {
    watching = true;
});

gulp.task('js', function() {
    var opts = {
        entries: './' + paths.js[0].src + 'app.js', // browserify requires relative path
        paths: [__dirname + '/node_modules', __dirname + '/app_lss_core/cartridge/'], //add search path's
        debug: gutil.env.sourcemaps
    };

    if (watching) {
        opts = xtend(opts, watchify.args);
    }

    var bundler = browserify(opts);
    if (watching) {
        bundler = watchify(bundler);
    }

    // optionally transform
    // bundler.transform('transformer');

    bundler.on('update', function(ids) {
        gutil.log('File(s) changed: ' + gutil.colors.cyan(ids));
        gutil.log('Rebundling...');
        rebundle();
    });

    bundler.on('log', gutil.log);

    function rebundle() {
        return bundler.bundle()
            .on('error', function(e) {
                gutil.log('Browserify Error', gutil.colors.red(e));
            })
            .pipe(source('app.js'))
            .pipe(buffer())
            .pipe(gulpif(argv.uglify, uglify()))
            .pipe(gulpif(argv.sourcemaps, sourcemaps.init({
                loadMaps: true
            })))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(paths.js[0].dest));
    }

    return rebundle();
});

gulp.task('bundle', ['js'], function() {
    return gulp.src([
        paths.lib + 'jquery/jquery-2.1.1.min.js',
        paths.lib + 'jquery/ui/jquery-ui.min.js',
        paths.lib + 'jquery/jquery.validate.min.js',
        paths.lib + 'jquery/slick.min.js',
        paths.lib + 'jquery/cloudzoom.js',
        paths.lib + 'jquery/jquery.nanoscroller.min.js',
        paths.lib + 'jquery/jquery.selectric.min.js',
        paths.lib + 'alloy_finger.js',
        paths.js[0].dest + 'app.js'
    ])
    .pipe(buffer())
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.js[0].dest));
});

// Static images optimization
var imagemin = require('gulp-imagemin');

gulp.task('images', () =>
    gulp.src(paths.assets + '/**/*')
    .pipe(imagemin([
        imagemin.gifsicle({
            interlaced: true
        }),
        imagemin.jpegtran({
            progressive: true
        }),
        imagemin.optipng({
            optimizationLevel: 5
        }),
        imagemin.svgo({
            plugins: [{
                removeViewBox: true
            }, {
                cleanupIDs: false
            }]
        })
    ]))
    .pipe(gulp.dest(paths.assets))
);

var eslint = require('gulp-eslint');
gulp.task('lint', function() {
    return gulp.src(['cartridges/app_lss_core/cartridge/js/**/*.js',
            'cartridges/app_lss_core/cartridge/scripts/**/*.js',
            'cartridges/app_lss_controllers/cartridge/controllers/**/*.js',
            'cartridges/app_lss_controllers/cartridge/scripts/**/*.js',
            'cartridges/int_lssblog/cartridge/controllers/**/*.js',
            'cartridges/int_lssblog/cartridge/scripts/**/*.js',
            'cartridges/bc_csc_customization/cartridge/scripts/**/*.js',
            'cartridges/int_instagram/cartridge/controllers/**/*.js',
            'cartridges/int_instagram/cartridge/scripts/**/*.js',
            'cartridges/bc_csc_actions/cartridge/scripts/**/*.js',
            'cartridges/bc_csc_actions/cartridge/controllers/**/*.js',
            'cartridges/bc_csc_actions/cartridge/static/default/js/**/*.js',
            'cartridges/int_xiatech/cartridge/scripts/**/*.js',
            'cartridges/int_xiatech/cartridge/controllers/**/*.js',
            'cartridges/int_bronto/cartridge/scripts/**/*.js'
        ])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('concatSystemObjects', function() {
  return gulp.src('./site/systemobjectextension/*.xml')
    .pipe(concat('system-objecttype-extensions.xml', {process: function(src) {
        return src.replace(/<\?xml version="1.0" encoding="UTF-8"\?>/g, '')
                    .replace(/<metadata xmlns="http:\/\/www.demandware.com\/xml\/impex\/metadata\/2006-10-31">/g, '')
                    .replace(/<\/metadata>/g, '')
                    .trim();
    }}))
    .pipe(concat.header(`<?xml version="1.0" encoding="UTF-8"?>\n<metadata xmlns="http://www.demandware.com/xml/impex/metadata/2006-10-31">\n`))
    .pipe(concat.footer(`\n</metadata>`))
    .pipe(gulp.dest('./site'));
});

gulp.task('concatSitePreferencesAttributes', function() {
    return gulp.src('./site/systemobjectextension/sitepreferences/metadata.sitepreferences.*.xml')
        .pipe(concat('sitePreferenceAttributes.xml', {process: function(src) {
            return src.replace(/<\?xml version="1.0" encoding="UTF-8"\?>/g, '')
                    .replace(/<metadata xmlns="http:\/\/www.demandware.com\/xml\/impex\/metadata\/2006-10-31">/g, '')
                    .replace(/<\/metadata>/g, '')
                    .match(/<custom-attribute-definitions>(.|\n)*?<\/custom-attribute-definitions>/gm)[0]
                    .replace(/<custom-attribute-definitions>/g, '')
                    .replace(/<\/custom-attribute-definitions>/g, '')
                    .trim();
        }}))
        .pipe(concat.header(`<custom-attribute-definitions>\n`))
        .pipe(concat.footer(`\n</custom-attribute-definitions>`))
        .pipe(gulp.dest('./site/systemobjectextension/sitepreferences'));
});

gulp.task('concatSitePreferencesGroups', function() {
    return gulp.src('./site/systemobjectextension/sitepreferences/metadata.sitepreferences.*.xml')
        .pipe(concat('sitePreferenceGroups.xml', {process: function(src) {
            return src.replace(/<\?xml version="1.0" encoding="UTF-8"\?>/g, '')
                    .replace(/<metadata xmlns="http:\/\/www.demandware.com\/xml\/impex\/metadata\/2006-10-31">/g, '')
                    .replace(/<\/metadata>/g, '')
                    .match(/<group-definitions>(.|\n)*?<\/group-definitions>/gm)[0]
                    .replace(/<group-definitions>/g, '')
                    .replace(/<\/group-definitions>/g, '')
                    .trim();
        }}))
        .pipe(concat.header(`<group-definitions>\n`))
        .pipe(concat.footer(`\n<\/group-definitions>`))
        .pipe(gulp.dest('./site/systemobjectextension/sitepreferences'));
});

gulp.task('concatSitePreferences', function() {
    return gulp.src('./site/systemobjectextension/sitepreferences/sitePreference*.xml')
        .pipe(concat('metadata.siteprefereces.xml', {process: function(src) {
            return src;
        }}))
        .pipe(concat.header(`<?xml version="1.0" encoding="UTF-8"?>\n<metadata xmlns="http://www.demandware.com/xml/impex/metadata/2006-10-31">\n<type-extension type-id="SitePreferences">\n`))
        .pipe(concat.footer(`\n</type-extension>\n</metadata>`))
        .pipe(gulp.dest('./site/systemobjectextension'));
});

gulp.task('concatSO', ['concatSitePreferencesAttributes', 'concatSitePreferencesGroups', 'concatSitePreferences', 'concatSystemObjects']);

gulp.task('build', ['bundle', 'css', 'images']);

gulp.task('default', ['enable-watch-mode', 'js', 'css'], function() {
    gulp.watch(paths.css.map(function(path) {
        return path.src + '**/*.scss';
    }), ['css']);
});
